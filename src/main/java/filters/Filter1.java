package filters;

import javax.servlet.*;
import java.io.IOException;

public class Filter1 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Filter is working");
        filterChain.doFilter(servletRequest, servletResponse);
        System.out.println("Filter still working");
    }

    @Override
    public void destroy() {

    }
}
