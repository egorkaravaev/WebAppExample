package util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class Util {
    private static Context context = null;
    private static DataSource dataSource = null;

    public static Connection getConnection () throws NamingException, SQLException {
        if(context == null){
            context = new InitialContext();
        }
        if(dataSource == null){
            dataSource = (DataSource) context.lookup("java:comp/env/jdbc/TestDB");
        }
        return dataSource.getConnection();
    }
}
