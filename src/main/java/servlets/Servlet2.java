package servlets;

import dao.AuthorDAO;
import dao.BookDAO;
import dao.GenreDAO;
import entity.Author;
import entity.Book;
import entity.Genre;
import org.apache.log4j.Logger;
import util.Util;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "Servlet2", urlPatterns = "/Servlet2")
public class Servlet2 extends HttpServlet{

    private final static Logger logger = Logger.getLogger(Servlet2.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("windows-1251");
        Connection connection = null;
        try {
            GenreDAO genreDAO = new GenreDAO();
            AuthorDAO authorDAO = new AuthorDAO();
            connection = Util.getConnection();
            List<Genre> genres = genreDAO.getAll(connection);
            req.setAttribute("genreList", genres);
            List<Author> authors = authorDAO.getAll(connection);
            req.setAttribute("authorList", authors);
        } catch (NamingException e) {
            logger.error("Something wrong with NamingException! " + e);
        } catch (SQLException e) {
            logger.error("Something wrong with SQL! " + e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Something wrong with SQL! " + e);
                }
            }
        }
        req.getRequestDispatcher("books2.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("windows-1251");
        Connection connection = null;
        try {
            connection = Util.getConnection();
            tryToAddBook(req,connection);
        } catch (NamingException e) {
            logger.error("Something wrong with NamingException! " + e);
        } catch (SQLException e) {
            logger.error("Something wrong with SQL! " + e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Something wrong with SQL! " + e);
                }
            }
        }
        req.getRequestDispatcher("books2.jsp").forward(req, resp);
    }

    private void tryToAddBook(HttpServletRequest request, Connection connection) {
        String name = request.getParameter("name");
        String pages = request.getParameter("pages");
        String year = request.getParameter("year");
        String gnr = request.getParameter("genre");
        String author = request.getParameter("author");
        if(name != "" && pages != "" && year != ""){
            int pgs = Integer.parseInt(pages);
            String[] a = author.split(" ");
            Author author1 = new Author(a[0],a[1]);
            Genre genre = new Genre(gnr);
            Book book = new Book(name, pgs, year, genre, author1);
            BookDAO bookDAO = new BookDAO();
            try {
                bookDAO.create(book,connection);
            } catch (SQLException e) {
                logger.error("Something wrong with SQL! " + e);
            }
        }
    }
}
