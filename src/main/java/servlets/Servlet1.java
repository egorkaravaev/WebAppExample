package servlets;

import dao.BookDAO;
import entity.Book;
import org.apache.log4j.Logger;
import util.Util;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "Servlet1", urlPatterns = "/")
public class Servlet1 extends HttpServlet{

    private final static Logger logger = Logger.getLogger(Servlet1.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection connection = null;
        try {
            BookDAO bookDAO = new BookDAO();
            connection = Util.getConnection();
            tryToDelete(req, connection, bookDAO);
            List<Book> bookList = bookDAO.getAll(connection);
            req.setAttribute("bookList", bookList);
        } catch (SQLException e) {
            logger.error("Something wrong with SQL! " + e);
        } catch (NamingException e) {
            logger.error("Something wrong with NamingException! " + e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Something wrong with SQL! " + e);
                }
            }
        }
        req.getRequestDispatcher("books.jsp").forward(req, resp);
    }

    private void tryToDelete(HttpServletRequest req, Connection connection, BookDAO bookDAO) {
        String bookID = req.getParameter("book_id");
        if(bookID != null){
            long id = Long.parseLong(bookID);
            try {
                bookDAO.delete(id,connection);
            } catch (SQLException e) {
                logger.error("Something wrong with SQL! " + e);
            }
        }
    }
}
