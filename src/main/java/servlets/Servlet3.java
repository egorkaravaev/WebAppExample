package servlets;

import dao.AuthorDAO;
import dao.BookDAO;
import dao.GenreDAO;
import entity.Author;
import entity.Book;
import entity.Genre;
import org.apache.log4j.Logger;
import util.Util;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "Servlet3", urlPatterns = "/Servlet3")
public class Servlet3 extends HttpServlet{

    private final static Logger logger = Logger.getLogger(Servlet3.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("windows-1251");
        Long id = Long.parseLong(req.getParameter("book_id"));
        Connection connection = null;
        GenreDAO genreDAO = new GenreDAO();
        AuthorDAO authorDAO = new AuthorDAO();
        BookDAO bookDAO = new BookDAO();
        Book book;
        try {
            connection = Util.getConnection();
            book = bookDAO.getById(id, connection);
            req.setAttribute("book", book);
            List<Genre> genres = genreDAO.getAll(connection);
            req.setAttribute("genreList", genres);
            List<Author> authors = authorDAO.getAll(connection);
            req.setAttribute("authorList", authors);
        } catch (NamingException e){
            logger.error("Something wrong with NamingException! " + e);
        } catch (SQLException e){
            logger.error("Something wrong with SQL! " + e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Something wrong with SQL! " + e);
                }
            }
        }
        req.getRequestDispatcher("books3.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("windows-1251");
        Connection connection = null;
        try {
            connection = Util.getConnection();
            tryToUpdate(req,connection);
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Something wrong with SQL! " + e);
                }
            }
        }
        req.getRequestDispatcher("books3.jsp").forward(req,resp);
    }

    private void tryToUpdate(HttpServletRequest req, Connection connection) {
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String pages = req.getParameter("pages");
        String year = req.getParameter("year");
        String gnr = req.getParameter("genre");
        String author = req.getParameter("author");
        if(name != "" && pages != "" && year != ""){
            Long ID = Long.parseLong(id);
            int pgs = Integer.parseInt(pages);
            AuthorDAO authorDAO = new AuthorDAO();
            GenreDAO genreDAO = new GenreDAO();
            BookDAO bookDAO = new BookDAO();
            String[] a = author.split(" ");
            try {
                long authorId = authorDAO.selectAId(a[0], a[1], connection);
                long genreId = genreDAO.selectGId(gnr, connection);
                Genre genre = new Genre(genreId, gnr);
                Author author1 = new Author(authorId, a[0], a[1]);
                Book book = new Book(ID, name, pgs, year, genre, author1);
                bookDAO.update(book, connection);
            } catch (SQLException e) {
                logger.error("Something wrong with SQL! " + e);
            }
        }
    }
}
