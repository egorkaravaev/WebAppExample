package tags;

import dao.BookDAO;
import util.Util;

import javax.naming.NamingException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

public class MyTag extends SimpleTagSupport{
    private String showCount;
    private String message;

    StringWriter writer = new StringWriter();

    public void setMessage(String message) {
        this.message = message;
    }

    public void setShowCount(String showCount) {
        this.showCount = showCount;
    }

    @Override
    public void doTag() throws JspException, IOException {
        if("true".equals(showCount)){
            try {
                Connection connection = Util.getConnection();
                BookDAO bookDAO = new BookDAO();
                int count = bookDAO.countBooks(connection);
                getJspBody().invoke(writer);
                getJspContext().getOut().println(writer.toString());
                JspWriter out = getJspContext().getOut();
                out.println(count);
            } catch (NamingException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else {
            JspWriter out = getJspContext().getOut();
            out.println("Не хотите, как хотите");
        }
    }
}
